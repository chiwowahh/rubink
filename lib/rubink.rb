  #!/usr/bin/env ruby
  ################################################################################
  # Rubink (Ruby Ink)
  # Author: Peter Lamber
  #
  # Usage:
  # rubink "path_to_your_script.rb"
  #
  # Example:
  # bin/rubink example/wormpainter.rb
  #
  # Does 2 things:
  # Compiles the ruby script into javascript
  # Creates a main_your_script_name.html with compiled script ready to run
  #
  # CAUTION: Generated files overwrite previous script if the same name was given
  #
  ################################################################################

  require 'opal'

  def build(script)
    @script = script

    return if invalid

    @app_name = File.basename(script,"rb")

    root = File.expand_path '../..', __FILE__

    Opal.append_path "."
    Opal.append_path root+"/lib"

    src = ""

    #Adding Libraries first
    src << IO.binread(root+"/lib/processing.js")
    src << IO.binread(root+"/lib/hammer.js")

    #Compile Rubink, script and wire it up
    src << Opal::Builder.build("app").to_s
    src << Opal::Builder.build(script).to_s
    src << Opal::Builder.build("start").to_s

    File.binwrite "app_#{@app_name}js", src
    File.binwrite "main_#{@app_name}html", main_html_template
    puts "main_#{@app_name}html was generated"
  end

  def invalid
    if @script.nil?
      puts "Stopping, no script given, provide script path"
      return true
    end

    unless File.exist?(@script)
      puts "Stopping, invalid filename"
      return true
    end
  end

  def main_html_template
    "<meta charset='utf-8'> <canvas style='outline:none' id='canvas'></canvas> <script src='app_#{@app_name}js'></script>"
  end

  build(ARGV[0])