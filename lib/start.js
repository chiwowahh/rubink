var canvas = document.getElementById('canvas');

function sketchProc(processing) {

  window.p = processing

  var a = Opal.App.$new();

  window.a = a

  processing.setup = function() {
    a.$setup();
  };

  processing.draw = function() {
    a.$draw();
  };

  processing.mousePressed = function() {
   a.$mousePressed();
  };

 processing.mouseReleased = function() {
   a.$mouseReleased();
  };

  processing.mouseClicked = function() {
   a.$mouseClicked();
  };

  processing.mouseMoved = function() {
   a.$mouseMoved();
  };

  processing.keyPressed = function() {
   a.$keyPressed();
  };


}

  window.sketchProc = sketchProc
  var mc = new Hammer(canvas);

  mc.on("panmove", function(ev) {
     a.$drag(ev.center.x,ev.center.y);
  });

  new Processing(canvas, window.sketchProc);
