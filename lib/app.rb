####################################
# Author: Peter Lamber
# Rubink (Ruby Ink)
# Made with Opal and Processing 
####################################

require 'opal'
require 'native'


module Common
  def ink_setup
    $window = Native(`window`)  # Get Page Window
    $p = $window.p              #Set global Processing hook

  end

  def alert(message)
    $window.alert(message)
  end 
  
  def black
    $p.background(1)
  end

# Check if point is over given rectangle
  def over?(x,y,l,h)
    true if $p.mouseX >= x &&
            $p.mouseX <= l+x &&
            $p.mouseY >= y &&
            $p.mouseY <= h+y      
  end

end


class Rubink
  include Common
  attr_accessor :p, :window, :dragX, :dragY, :sprites
  def initialize
    ink_setup
    @p = $p
    @window = $window
    @sprites = [] 
    @m = 0 
    @sprite = 0
  end
  
  
  def renderSprite(milisec, x, y, w, h)
    @p.image(@sprites[@sprite],x,y,w,h)
    @sprite += 1  if passMili?(milisec) 
    @sprite = 0 if @sprites.length == @sprite
  end
  
  def pushSprite(sprite)
    @sprites.push(@p.loadImage(sprite))
  end
  
  def drag(x,y) 
    @dragX,@dragY = x,y
  end
  
  def passMili?(time)
    @m = @p.millis if @m == 0
    @m2 = @p.millis 
    @m3 = @m2 - @m 
    if @m3 >= time
      @m,@m2 = 0,0
      return true 
    else
      return false
    end
  end
  
  def mousePressed   
  end
  
  def mouseReleased
  end
  
  def mouseClicked
  end
  
  def mouseMoved
  end
  
  def keyPressed  
  end
end

