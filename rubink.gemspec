Gem::Specification.new do |s|
  s.name        = 'rubink'
  s.version     = '0.0.1'
  s.executables << 'rubink'
  s.date        = '2016-09-09'
  s.summary     = "Rubink"
  s.description = "Write animations with ruby"
  s.authors     = ["Peter Lamber"]
  s.email       = 'peter.lamber@protonmail.com'
  s.require_paths = ["lib"]
  s.add_dependency 'opal', '~> 0.10.0'
  s.files        = Dir.glob("{bin,lib}/**/*")
  s.homepage    =
    'http://rubygems.org/gems/rubink'
  s.license       = 'MIT'
end