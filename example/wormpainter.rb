###
# Small example
# Press and drag the mouse to paint

class App < Rubink
  def setup
    # use @p to call into processing
    @p.smooth
    @p.size( @window.innerWidth, @window.innerHeight)
    @p.background(255)
    @s,@i = 10,1
  end

  def draw
    @p.ellipse @dragX,@dragY,@s,@s
    @s += @i 
    @i *= -1 if @s == 30 || @s == 0
  end
end